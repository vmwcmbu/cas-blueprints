# VMware Cloud Management Cloud Automation Services Demo Blueprints 

Attached is a collection of demonstration blueprints for Cloud Automation Services. Each blueprint contains specific samples of blueprint capabilities. 

Suggested to fork this blueprint into your own repository and bind it to your projects in Cloud Assembly

